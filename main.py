from jug import Jug
from state import State


def main():
    jug_a, jug_b = Jug(4), Jug(3)
    actions = {
        'Empty A': lambda a, b: a.empty(),
        'Empty B': lambda a, b: b.empty(),
        'Fill A': lambda a, b: a.fill(),
        'Fill B': lambda a, b: b.fill(),
        'Transfer from A to B': lambda a, b: a.transfer_to(b),
        'Transfer from B to A': lambda a, b: b.transfer_to(a)
    }

    initial = State(jug_a, jug_b, 'Initial state')
    visited = [initial]
    path = search_path(initial, actions, visited)

    print(initial)
    for step in path:
        print(step)


def search_path(initial_state, actions, visited_states):
    path = []
    if not initial_state.is_desired():
        adjacent_states = initial_state.adjacent(actions, visited_states)
        visited_states.extend(adjacent_states)
        for state in adjacent_states:
            if state.is_desired():
                path.append(state)
                break
            else:
                searched_path = search_path(state, actions, visited_states)
                if searched_path:
                    path.append(state)
                    path.extend(searched_path)
                    break
    return path


if __name__ == '__main__':
    main()
