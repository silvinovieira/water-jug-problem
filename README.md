# The Water Jug Problem

Suppose you are given two jugs: a 4-gallon one and 3-gallon one, and neither has any markings on it.

You have a pump that can be used to fill the jugs with water.

What do you do so that exactly 2 gallons of water are left in the 4 gallon jug?
