from copy import copy


class State:

    def __init__(self, jug_a, jug_b, action):
        self.a = jug_a
        self.b = jug_b
        self.action = action

    def __eq__(self, other):
        return self.a.volume == other.a.volume and self.b.volume == other.b.volume

    def __str__(self):
        return '({}, {}) <- {}'.format(self.a.volume, self.b.volume, self.action)

    def adjacent(self, actions, visited_states):
        states = []
        for key in actions.keys():
            a_copy, b_copy = copy(self.a), copy(self.b)
            actions[key](a_copy, b_copy)
            state = State(a_copy, b_copy, key)
            if state not in visited_states:
                states.append(state)
        return states

    def is_desired(self):
        return self.a.volume == 2
