class Jug:

    def __init__(self, max):
        self.max = max
        self.volume = 0

    def empty(self):
        self.volume = 0

    def fill(self):
        self.volume = self.max

    def transfer_to(self, other_jug):
        total_volume = self.volume + other_jug.volume
        if total_volume <= other_jug.max:
            self.volume = 0
            other_jug.volume = total_volume
        else:
            to_transfer = other_jug.max - other_jug.volume
            self.volume -= to_transfer
            other_jug.volume += to_transfer
